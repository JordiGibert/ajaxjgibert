package com.Ajax.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Ajax.dto.Products;
import com.Ajax.service.ProductsServiceImpl;



@RestController
@CrossOrigin(origins ="*"/*, methods= {RequestMethod.GET,RequestMethod.POST}*/)
@RequestMapping(value = "/api")
public class ProductsController {
	@Autowired
	ProductsServiceImpl productsServiceImpl; 
	
	@GetMapping("/products")
	public List<Products> listarProducts(){
		return productsServiceImpl.listCategoria();
	}
	
	
	@PostMapping("/products")
	public Products salvarProducts(@RequestBody Products products) {
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
		Date date = new Date(System.currentTimeMillis());
		products.setUpdated_at(date);
		products.setCreated_at(date);
		return productsServiceImpl.saveProducts(products);
	}
	
	
	@GetMapping("/products/{id}")
	public Products productsXID(@PathVariable(name="id") Integer id) {
		
		Products products_xid= new Products();
		
		products_xid=productsServiceImpl.productsXID(id);
		
		System.out.println("Products XID: "+products_xid);
		
		return products_xid;
	}
	
	@PutMapping("/products/{id}")
	public Products actualizarProducts(@PathVariable(name="id")Integer id,@RequestBody Products products) {
		
		Products products_seleccionado= new Products();
		Products products_actualizado= new Products();
		
		products_seleccionado= productsServiceImpl.productsXID(id);
		
		products_seleccionado.setName(products.getName());
		
		products_seleccionado.setDetail(products.getDetail());
		
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
		Date date = new Date(System.currentTimeMillis());
		products_seleccionado.setUpdated_at(date);
		
		products_actualizado = productsServiceImpl.updateProducts(products_seleccionado);
		
		System.out.println("El producto actualizada es: "+ products_actualizado);
		
		return products_actualizado;
	}
	
	@DeleteMapping("/products/{id}")
	public void eliminarProducts(@PathVariable(name="id")Integer id) {
		productsServiceImpl.deleteProducts(id);
	}
}
