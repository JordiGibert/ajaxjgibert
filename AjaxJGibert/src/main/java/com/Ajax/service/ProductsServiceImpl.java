package com.Ajax.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Ajax.dao.IProductsDAO;
import com.Ajax.dto.Products;



@Service
public class ProductsServiceImpl implements IProductsService{

	@Autowired
	IProductsDAO iProductsDAO;

	@Override
	public List<Products> listCategoria() {
		// TODO Auto-generated method stub
		return iProductsDAO.findAll();
	}

	@Override
	public Products saveProducts(Products products) {
		// TODO Auto-generated method stub
		return iProductsDAO.save(products);
	}

	@Override
	public Products productsXID(Integer id) {
		// TODO Auto-generated method stub
		return iProductsDAO.findById(id).get();
	}

	@Override
	public Products updateProducts(Products products) {
		// TODO Auto-generated method stub
		return iProductsDAO.save(products);
	}

	@Override
	public void deleteProducts(Integer id) {
		// TODO Auto-generated method stub
		iProductsDAO.deleteById(id);
	}
	
}
