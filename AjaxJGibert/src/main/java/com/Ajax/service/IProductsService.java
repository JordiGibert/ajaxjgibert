package com.Ajax.service;

import java.util.List;

import com.Ajax.dto.Products;



public interface IProductsService {
public List<Products> listCategoria(); 
	
	public Products saveProducts(Products products);	
	
	public Products productsXID(Integer id); 
	
	public Products updateProducts(Products products); 
	
	public void deleteProducts(Integer id);
}
