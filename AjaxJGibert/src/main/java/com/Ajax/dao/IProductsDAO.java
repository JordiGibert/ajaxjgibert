package com.Ajax.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Ajax.dto.Products;


public interface IProductsDAO extends JpaRepository<Products, Integer>{

}
