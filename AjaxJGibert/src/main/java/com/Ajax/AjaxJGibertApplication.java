package com.Ajax;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AjaxJGibertApplication {

	public static void main(String[] args) {
		SpringApplication.run(AjaxJGibertApplication.class, args);
	}

}
