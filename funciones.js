function getAll() {
    $.ajax({
    url: "http://localhost:8181/api/products",
    method: 'GET',
    dataType: 'json',
    headers: {
        'Accept':'application/json'
    },
    success: function (data) {
        alert("id del primer dato: "+data[0].id+"\nNombre del primer dato: "+data[0].name);
        alert(JSON.stringify(data));
    },

    error: function (error) {
        alert(error);
    }
    });
}

function postProduct() {
    var nom= $("#name").val();
    var detall= $("#detail").val();
    if(nom=="") {
        alert("El camp nom no pot estar buit")
    } else {

    

    $.ajax({
        url: "http://localhost:8181/api/products",
        method: 'POST',
       
        dataType: 'json',
        headers: {
            'Accept':'application/json',
            'Content-Type': 'application/json' 
        },
        //contentType: 'application/x-www-form-urlencoded',
        data: JSON.stringify({ "name": nom, "detail": detall }),

        success: function (data, textStatus, jqXHR) {
            alert("Producto registrado!");
            alert(JSON.stringify(data));
        },
    
        error: function (error) {
            alert(error+"error");
        }
        });
    }
}

function searchProduct() {
    var id=$("#id").val();
    if(id=="") {
        alert("El camp id no pot estar buit")
    } else {
    $.ajax({
        url: "http://localhost:8181/api/products/"+id,
        method: 'GET',
        dataType: 'json',
        headers: {
            'Accept':'application/json'
        },
        success: function (data) {
            $("#nameE").val(data.name);
            $("#detailE").val(data.detail);
            console.log("Campos impresos");
            console.log(JSON.stringify(data)); 
        },
    
        error: function (error) {
            alert(error);
        }
        });
    }
}

function putProduct() {
    var id=$("#id").val();
    var nom= $("#nameE").val();
    var detall= $("#detailE").val();
    if(nom=="" || id=="")  {
        alert("El camp nom o id no pot estar buit")
    } else {
        $.ajax({
            url:  "http://localhost:8181/api/products/"+id,
            method: 'PUT',
           
            dataType: 'json',
            headers: {
                'Accept':'application/json',
                'Content-Type': 'application/json' 
            },
            //contentType: 'application/x-www-form-urlencoded',
            data: JSON.stringify({ "name": nom, "detail": detall }),
    
            success: function (data, textStatus, jqXHR) {
                alert("Producto actualizado!");
                alert(JSON.stringify(data));
            },
        
            error: function (error) {
                alert(error+"error");
            }
            });
        }
}

function deleteProduct() {
    var id=$("#idDel").val();
    if(id=="") {
        alert("El camp id no pot estar buit")
    } else {
    $.ajax({
        url: "http://localhost:8181/api/products/"+id,
        method: 'DELETE',
        dataType: 'json',
        headers: {
            'Accept':'application/json'
        },
        success: function (data) {
            alert("Producte eliminat")
        },
    
        error: function (error) {
            alert(error);
        }
        });
    }
}